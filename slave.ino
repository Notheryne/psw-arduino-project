/*
 * AUTHOR: Bartosz Rydziński
 * This code is meant to be run on slave Arduino.
 */
 // Library imports
 
#include <SoftwareWire.h> // this is a library for software handled I2C communication
SoftwareWire myWire(A4, A5); // Initialize A4 and A5 as Software I2C
#include <Wire.h>
#define myWire Wire // Because I2C uses A4 and A5 it's easy to switch between HW and SW way

// Pin definitions
int ROWS[] = {2,7,A3,5,13,A2,12,A0}; // Pins responsible for rows
int COLS[] = {6,11,10,3,A1,4,8,9}; // Pins responsible for columns

// Master-slave communication definitions
#define SLAVE_ADDRESS 9
#define ANSWER_SIZE 5
#define NUM_READINGS 8
#define PLOT_TIME 8 // in seconds

// Debug definition
#define DEBUG true

// Variable initializations
int receiveCounter = 0;
int sensorReadings[8] = {0, 0, 0, 0, 0, 0, 0, 0};
String answer = "SLAVE";


void setup() {
  Wire.begin(SLAVE_ADDRESS);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);

  if (DEBUG) Serial.begin(9600);
  if (DEBUG) Serial.println("This is Slave Arduino");

  // Initialize LED Matrix pins as OUTPUT
  for (int i = 0; i < 8; i++) {
    pinMode(ROWS[i], OUTPUT);
    pinMode(COLS[i], OUTPUT);
  }
}

void receiveEvent() {
  if (receiveCounter == NUM_READINGS) {
    receiveCounter = 0;
    for (int i = 0; i < NUM_READINGS; i++) {
      sensorReadings[i] = 0;
    }
  }
  while (0 < Wire.available()) {
    byte x = Wire.read();
    sensorReadings[receiveCounter] = x;
    receiveCounter += 1;
  }
  
  if (DEBUG) Serial.println("Received:");
  if (DEBUG) {
    for (int i = 0; i < NUM_READINGS - 1; i++) {
      Serial.print(sensorReadings[i]);
      Serial.print(", ");
    }
    Serial.println(sensorReadings[NUM_READINGS - 1]);
  }
}

void requestEvent() {
  byte response[ANSWER_SIZE];
  // Convert previously declared answer ("SLAVE") to bytes
  for(byte i = 0; i < ANSWER_SIZE; i++) {
    response[i] = (byte) answer.charAt(i);
  }

  // Send answer
  Wire.write(response, sizeof(response));

  if (DEBUG) Serial.println("Sending answer \"" + answer + '"');
}

void displayPlot() {
  /*
   * Sensor maximum readings are 90 for humidity and 50 for temperature,
   * so `minimum` is initialized to 100 just in case.
   */
  double minimum = 100.0;
  double maximum = -1.0;
  int minimum_index;
  int maximum_index;
  
  // Find extreme values
  for (int i = 0; i < NUM_READINGS; i++) {
    if (sensorReadings[i] < minimum) {
      minimum = sensorReadings[i];
      minimum_index = i;
    }
    if (sensorReadings[i] > maximum) {
      maximum = sensorReadings[i];
      maximum_index = i;
    }
  }
  
  // Find interval (unit represented by single row)
  double interval = (maximum - minimum) / NUM_READINGS;

  int positions[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  
  // Find how many intervals above minimum is each reading
  if (interval != 0) {
    for(int i = 0; i < NUM_READINGS; i++) {
      int intervals = 0;
      if (sensorReadings[i] != minimum) {
        intervals = floor((sensorReadings[i] - minimum) / interval);
      }
      if (intervals >= 8) { intervals = 7; } // in case of precision issue
      positions[i] = intervals;
    }
  }
  if (DEBUG) {
  Serial.print("Positions:");
    for(int i = 0; i < 8; i++) {
      Serial.print(positions[i]);
      Serial.print(", ");
    }
    Serial.print('\n');
  }
  
  // Initialize Matrix State
  unsigned char plot_data[8][8] = {
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0}
  };

  // Populate Matrix state
  for (int i = 0; i < NUM_READINGS; i++) {
    plot_data[7 - positions[i]][i] = 1;
  }

  if (DEBUG) {
    Serial.println("The matrix plot data: ");
    for(int i = 0; i < 8; i++) {
      for(int j = 0; j < 7; j ++) {
        Serial.print(plot_data[i][j]);
        Serial.print(", ");
      }
      Serial.print(plot_data[i][7]);
      Serial.print('\n');
    }
  }

  // Display matrix for PLOT_TIME * 1000 / 30 times,
  // 30 here is about the time in miliseconds that displaying takes
  for (int i = 0; i < (PLOT_TIME * 1000) / 30; i++) {
    displayOnMatrix(plot_data);
  }
  dimAllLeds(); // dim the leds
}

void displayOnMatrix(unsigned char dat[8][8]) {
  // The state of LED Matrix is an unsigned char [8][8]
  // With 0 for a diode that's not lit and 1 otherwise
  for(int c = 0; c<8;c++) {
    // Iterate over columns
    digitalWrite(COLS[c], LOW);
    // Then over rows
    for(int r = 0;r<8;r++)  
    {  
      digitalWrite(ROWS[r], dat[r][c]);  
    }  
    delay(1);
    clearMatrix();
  }
}  

void clearMatrix() {  
  for(int i = 0;i<8;i++)  
  {  
    digitalWrite(ROWS[i],LOW);  
    digitalWrite(COLS[i],HIGH);  
  }  
}

void dimAllLeds() {
  unsigned char nothing[8][8] = {
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0}
  };
  displayOnMatrix(nothing);
}
  
void loop() {
  // if there are 8 readings stored, display plot of them.
  if (receiveCounter == NUM_READINGS) {
    displayPlot();
  }
  /*
  * Please note that there's no termination character and if you switch the Slave on
  * in between temperature and humidity readings,
  * the slave will not care and will display the plot for the data it has gotten
  */
  delay(50);
}
