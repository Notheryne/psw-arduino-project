/*
 * AUTHOR: Bartosz Rydziński
 * This code is meant to be run on master Arduino.
 */

// Library imports
#include <SoftwareWire.h> // this is a library for software handled I2C communication
                          // because when the Slave was using A4 and A5 the screen did not work



#include <LiquidCrystal_I2C.h>
#include <MyRealTimeClock.h>
#include <dht.h>

// Pin definitions
#define DHT_PIN 13
#define RTC_CLK_PIN 6
#define RTC_DAT_PIN 7
#define RTC_RST_PIN 8

// Master-slave communication definitions
#define SLAVE_ADDRESS 9
#define ANSWER_SIZE 5
#define NUM_READINGS 8 // this is defined to be constant as there's 8x8 LED matrix used to plot
#define PLOT_TIME 8 // in seconds

// Debug definition
#define DEBUG false

// Variable initializations
int lcd_address = 0x27;
int lcd_columns = 16;
int lcd_rows = 2;

SoftwareWire myWire(A0, A1); // Emulate A0 as SDA and A1 as SCL,
                             // internal pullups is turned on by default
                             // the Slave is allowed to stretch the clock
                             
LiquidCrystal_I2C LCD(0x27, lcd_columns, lcd_rows); // Initialize LCD on 0x27

MyRealTimeClock RTC(RTC_CLK_PIN, RTC_DAT_PIN, RTC_RST_PIN); // Initialize RTC on 6, 7, 8

dht DHT;

int time_between_readings = 5; // in seconds
int last_reading = -1;

int clock_screen_view_time = 15; // how long the time will be shown in seconds
                                 // if this is very short you need to power
                                 // slave before master
char *days_of_week[7] = {"Nd", "Pn", "Wt", "Sr", "Cz", "Pt", "So"};


void setup() {
  // LCD
  LCD.init();
  LCD.backlight();
  
  if (DEBUG) Serial.begin(9600);
  if (DEBUG) Serial.println("This is Master Arduino");
  // Communication
  myWire.begin();
  
  // Real Time CLOCK
  // To set current time and date, you pass data in this order:
  // Seconds 00, Minute 00, Hour 00, Day of week 0, Day 00, Month 00, Year 0000
   RTC.setDS1302Time(00, 34, 14, 0, 31, 05, 2020);
}

void loop() {
  // Real Time View
  for(int i = 0; i < clock_screen_view_time; i++) {;
    displayCurrentTime();
    delay(1000);
  }

  // Temperature View
  for(int i = 0; i < NUM_READINGS; i++) {
    DHT.read11(DHT_PIN);
    displayTemperature();
    delay(time_between_readings * 1000);
  }
  
  delay(PLOT_TIME * 1000 + 100);
  
  // Humidity View
  for(int i = 0; i < NUM_READINGS; i++) {
    DHT.read11(DHT_PIN);
    displayHumidity();
    delay(time_between_readings * 1000);
  }
  
  delay(PLOT_TIME * 1000 + 100);
}

void sendLastReading() {
  // Send data to slave
  if (DEBUG) Serial.println("Sending data to slave");
  myWire.beginTransmission(SLAVE_ADDRESS);
  myWire.write(last_reading);
  myWire.endTransmission();

  // Request response
  myWire.requestFrom(SLAVE_ADDRESS, ANSWER_SIZE);

  // Read response
  String response = "";
  while (myWire.available()) {
    char b = myWire.read();
    response += b;
  }

  // Show response if in debug mode
  if (DEBUG) Serial.println("The slave answered with:");
  if (DEBUG) Serial.println(response);
}

void displayTemperature() {
  LCD.clear();
  LCD.setCursor(0, 0);
  LCD.print("Temperatura wewn");

  last_reading = DHT.temperature;
  LCD.setCursor(5, 1);
  LCD.print(last_reading);
  LCD.print(' ');
  LCD.print((char)223);
  LCD.print('C');

  if (DEBUG) Serial.print("Sending data to slave");
  sendLastReading();
  
  if (DEBUG) {
    Serial.print("Temperatura: ");
    Serial.print(DHT.temperature);
    Serial.println('C');
  }
}

void displayHumidity() {
  LCD.clear();
  LCD.setCursor(3, 0);
  LCD.print("Wilgotnosc");

  last_reading = DHT.humidity;
  LCD.setCursor(6, 1);
  LCD.print(last_reading);
  LCD.print("%");

  if (DEBUG) Serial.print("Sending data to slave");
  sendLastReading();

  if (DEBUG) {
    Serial.print("Wilgotnosc: ");
    Serial.print(DHT.humidity);
    Serial.println('%');
  }
}

void displayCurrentTime() {
  RTC.updateTime();
  LCD.clear();
  
  LCD.setCursor(1, 0);
  LCD.print(days_of_week[RTC.dayofweek]);
  LCD.print("  "); // note there's a double space
  LCD.print(RTC.dayofmonth);
  LCD.print('.');
  LCD.print(RTC.month);
  LCD.print('.');
  LCD.print(RTC.year);
  
  LCD.setCursor(4, 1);
  LCD.print(RTC.hours);
  LCD.print(":");
  LCD.print(RTC.minutes);
  LCD.print(":");
  LCD.print(RTC.seconds);

  if (DEBUG) {
    Serial.print(days_of_week[RTC.dayofweek]);
    Serial.print("  "); // note there's a double space
    Serial.print(RTC.dayofmonth);
    Serial.print('.');
    Serial.print(RTC.month);
    Serial.print('.');
    Serial.print(RTC.year);
    Serial.println('.');
    
    Serial.print(RTC.hours);
    Serial.print(":");
    Serial.print(RTC.minutes);
    Serial.print(":");
    Serial.print(RTC.seconds);
  }
}
